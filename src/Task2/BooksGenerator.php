<?php

declare(strict_types=1);

namespace App\Task2;

class BooksGenerator
{
    private $minPagesNumber;
    private $libraryBooks;
    private $maxPrice;
    private $storeBooks;

    public function __construct(int $minPagesNumber, array $libraryBooks, int $maxPrice, array $storeBooks)
    {
        $this->minPagesNumber = $minPagesNumber;
        $this->libraryBooks = $libraryBooks;
        $this->maxPrice = $maxPrice;
        $this->storeBooks = $storeBooks;
    }

    public function generate(): \Generator
    {
        foreach ($this->libraryBooks as $libraryBooks){
           if($this->minPagesNumber <= $libraryBooks->getPagesNumber()){
               yield $libraryBooks;
           }
        }

        foreach ($this->storeBooks as $storeBooks){
            if($this->maxPrice >= $storeBooks->getPrice()){
                yield $storeBooks;
            }
        }
    }
}
