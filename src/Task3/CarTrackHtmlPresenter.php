<?php

declare(strict_types=1);

namespace App\Task3;

use App\Task1\Car;
use App\Task1\Track;

class CarTrackHtmlPresenter
{
    public function present(Track $track): string
    {
        $result = '';

        $cars = $track->all();

        if (!empty($cars)) {
            $result .= '
            <table>
                <thead>
                    <tr>
                        <th scope="col">Name</th>
                        <th scope="col">Image</th>
                    </tr>
                </thead>
                <tbody>
            ';

            foreach ($cars as $car) {
                $result .= "<tr>";
                $result .= "<td>" . $car->getName() . ": " . $car->getSpeed() . ", " . $car->getFuelConsumption() . "</td>";
                $result .= "<td><img src=\"" . $car->getImage() . "\"></td>";
                $result .= "<tr>";
            }

            $result .= '
                </tbody>
            </table>
            ';
        }

        return $result;
    }
}
