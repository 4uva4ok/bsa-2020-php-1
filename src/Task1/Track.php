<?php

declare(strict_types=1);

namespace App\Task1;

class Track
{
    private $cars;
    private $lapLength;
    private $lapsNumber;

    public function __construct(float $lapLength, int $lapsNumber)
    {
        $this->lapLength = $lapLength;
        $this->lapsNumber = $lapsNumber;
    }

    public function getLapLength(): float
    {
        return $this->lapLength;
    }

    public function getLapsNumber(): int
    {
        return $this->lapsNumber;
    }

    public function add(Car $car): void
    {
        $this->cars[] = $car;
    }

    public function all(): array
    {
        return $this->cars;
    }

    public function run(): Car
    {
        $track_length = $this->getLapLength() * $this->getLapsNumber();
        $track_cars = $this->all();

        $winner_time = null;
        $winner_car = null;

        foreach ($track_cars as $car) {
            $car_track_time = $track_length / $car->getSpeed();

            $car_fuel = $car->getFuelConsumption() * $track_length / 100;

            $count_pit_stop = ceil($car_fuel / $car->getFuelTankVolume());

            $total_time = $car_track_time * 60 * 60 + $count_pit_stop * $car->getPitStopTime();

            if (is_null($winner_time) || $winner_time > $total_time) {
                $winner_time = $total_time;
                $winner_car = $car;
            }
        }

        return $winner_car;
    }
}
